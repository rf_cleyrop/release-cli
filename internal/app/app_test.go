package app

import (
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	log, _ := testlog.NewNullLogger()
	testApp := New(log, "version")

	require.NotNil(t, testApp)
	require.Equal(t, "release-cli", testApp.Name)
	require.Len(t, testApp.Commands, 2)
	require.Len(t, testApp.Flags, 7)
}

func TestLogOutput(t *testing.T) {
	log, hook := testlog.NewNullLogger()
	testApp := New(log, "version")
	require.NotNil(t, testApp)

	testApp.Writer = log.Writer()
	args := []string{"release-cli", "--server-url", "s.url", "--job-token", "", "--project-id", "projectID"}

	tests := []struct {
		name            string
		extraArg        string
		expectedMessage string
	}{
		{
			name:            "correct_usage_no_command",
			expectedMessage: "release-cli - A CLI tool that interacts with GitLab's Releases API",
		},
		{
			name:            "incorrect_usage",
			extraArg:        "-incorrect",
			expectedMessage: "Incorrect usage",
		},
		{
			name:            "unknown_command",
			extraArg:        "unknown-command",
			expectedMessage: "Command not found: \"unknown-command\"",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			hook.Reset()
			testArgs := args
			if tt.extraArg != "" {
				testArgs = append(args, tt.extraArg)
			}

			err := testApp.Run(testArgs)
			require.NoError(t, err)
			require.True(t, assertContainsMessage(t, hook.AllEntries(), tt.expectedMessage))
		})
	}
}

func assertContainsMessage(t *testing.T, entries []*logrus.Entry, msg string) bool {
	t.Helper()

	for _, entry := range entries {
		if strings.Contains(entry.Message, msg) {
			return true
		}
	}

	return false
}
