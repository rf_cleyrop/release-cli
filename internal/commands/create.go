package commands

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	securejoin "github.com/cyphar/filepath-securejoin"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/release-cli/internal/flags"
	"gitlab.com/gitlab-org/release-cli/internal/gitlab"
)

type httpClientFn func(ctx *cli.Context, log logrus.FieldLogger) (gitlab.HTTPClient, error)

// Create defines the create command to be used by the CLI
func Create(log logrus.FieldLogger, httpClientFn httpClientFn) *cli.Command {
	return &cli.Command{
		Name:  "create",
		Usage: "Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release",
		Action: func(ctx *cli.Context) error {
			client, err := httpClientFn(ctx, log)
			if err != nil {
				return err
			}

			return createRelease(ctx, log, client)
		},
		Subcommands: nil,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     flags.Name,
				Usage:    "The release name",
				Required: false,
			},
			&cli.StringFlag{
				Name:     flags.Description,
				Usage:    "The description of the release; you can use Markdown. A file can be used to read the description contents, must exist inside the working directory; if it contains any whitespace, it will be treated as a string",
				Required: false,
			},
			&cli.StringFlag{
				Name:     flags.TagName,
				Usage:    "The tag the release will be created from",
				Required: true,
				EnvVars:  []string{"CI_COMMIT_TAG"},
			},
			&cli.StringFlag{
				Name:     flags.Ref,
				Usage:    "If tag_name doesn’t exist, the release will be created from ref; it can be a commit SHA, another tag name, or a branch name",
				Required: false,
				EnvVars:  []string{"CI_COMMIT_SHA"},
			},
			&cli.StringSliceFlag{
				Name:     flags.AssetsLinksName,
				Usage:    `[DEPRECATED use --assets-link instead] List of asset link names; must be used with --asset-links-url; ignored if --assets-link is defined (e.g. --assets-links-name "asset 1" --assets-links-url "https://example.com/url/1" --assets-links-name "asset 2" --assets-links-url "https://example.com/url/2")`,
				Required: false,
			},
			&cli.StringSliceFlag{
				Name:     flags.AssetsLinksURL,
				Usage:    `[DEPRECATED use --assets-link instead] List of asset link URLs; must be used with --asset-links-name; ignored if --assets-link is defined (e.g. --assets-links-name "asset 1" --assets-links-url "https://example.com/url/1" --assets-links-name "asset 2" --assets-links-url "https://example.com/url/2")`,
				Required: false,
			},
			&cli.StringSliceFlag{
				Name:     flags.AssetsLink,
				Usage:    `JSON string representation of an asset link; takes precedence over --assets-links-name and --assets-links-url (e.g. --assets-link='{"name": "Asset1", "url":"https://<domain>/some/location/1", "type": "other", "filepath": "xzy" }' or --assets-link='[{"name": "Asset1", "url":"https://example.com/some/location/1"}, {"name": "Asset2", "url":"https://example.com/some/location/2"}]'`,
				Required: false,
			},
			&cli.StringSliceFlag{
				Name:     flags.Milestone,
				Usage:    `List of the titles of each milestone the release is associated with (e.g. --milestone "v1.0" --milestone "v1.0-rc)"; each milestone needs to exist `,
				Required: false,
			},
			&cli.StringFlag{
				Name:     flags.ReleasedAt,
				Usage:    `The date when the release will be/was ready; defaults to the current time; expected in ISO 8601 format (2019-03-15T08:00:00Z)`,
				Required: false,
			},
		},
	}
}

func createRelease(ctx *cli.Context, log logrus.FieldLogger, httpClient gitlab.HTTPClient) error {
	projectID := ctx.String(flags.ProjectID)
	serverURL := ctx.String(flags.ServerURL)
	jobToken := ctx.String(flags.JobToken)
	privateToken := ctx.String(flags.PrivateToken)

	l := log.WithFields(logrus.Fields{
		"command":       ctx.Command.Name,
		flags.ServerURL: serverURL,
		flags.ProjectID: projectID,
		flags.Name:      ctx.String(flags.Name),
		flags.TagName:   ctx.String(flags.TagName),
		flags.Ref:       ctx.String(flags.Ref),
	})

	l.Info("Creating Release...")

	gitlabClient, err := gitlab.New(serverURL, jobToken, privateToken, projectID, httpClient)
	if err != nil {
		return fmt.Errorf("failed to create GitLab client: %w", err)
	}

	crr, err := newCreateReleaseReq(ctx, log)
	if err != nil {
		return fmt.Errorf("new CreateReleaseRequest: %w", err)
	}

	release, err := gitlabClient.CreateRelease(ctx.Context, crr)
	if err != nil {
		return fmt.Errorf("failed to create release: %w", err)
	}

	printReleaseOutput(ctx.App.Writer, release)

	l.Info("release created successfully!")

	return nil
}

func getDescription(description string, log logrus.FieldLogger) (string, error) {
	l := log.WithFields(logrus.Fields{
		"description": description,
	})

	if description == "" || strings.Contains(strings.TrimSpace(description), " ") {
		return description, nil
	}

	baseDir, err := os.Getwd()
	if err != nil {
		l.WithError(err).Warn("failed to get working directory, using string value for --description")
		return description, nil
	}

	filePath, err := securejoin.SecureJoin(baseDir, description)
	if err != nil {
		l.WithError(err).Warn("failed to resolve filepath, using string value for --description")
		return description, nil
	}

	content, err := os.ReadFile(filepath.Clean(filePath))
	if errors.Is(err, os.ErrNotExist) {
		l.WithError(err).Warn("file does not exist, using string value for --description")
		return description, nil
	}

	return string(content), err
}

func newCreateReleaseReq(ctx *cli.Context, log logrus.FieldLogger) (*gitlab.CreateReleaseRequest, error) {
	assetLinksNames := ctx.StringSlice(flags.AssetsLinksName)
	assetLinksURL := ctx.StringSlice(flags.AssetsLinksURL)
	assetsLink := ctx.StringSlice(flags.AssetsLink)
	description := ctx.String(flags.Description)
	releasedAt := ctx.String(flags.ReleasedAt)

	assets, err := gitlab.ParseAssets(assetLinksNames, assetLinksURL, assetsLink)
	if err != nil {
		return nil, fmt.Errorf("failed to parse assets: %w", err)
	}

	descriptionString, err := getDescription(description, log)
	if err != nil {
		return nil, err
	}

	crr := &gitlab.CreateReleaseRequest{
		ID:          ctx.String(flags.ProjectID),
		Name:        ctx.String(flags.Name),
		Description: descriptionString,
		TagName:     ctx.String(flags.TagName),
		Ref:         ctx.String(flags.Ref),
		Assets:      assets,
		Milestones:  ctx.StringSlice(flags.Milestone),
	}

	if releasedAt != "" {
		timeReleasedAt, err := gitlab.ParseDateTime(releasedAt)
		if err != nil {
			return nil, fmt.Errorf("failed to parse released-at: %w", err)
		}

		crr.ReleasedAt = &timeReleasedAt
	}

	return crr, nil
}

func printReleaseOutput(w io.Writer, release *gitlab.CreateReleaseResponse) {
	fmt.Fprintf(w, `

Tag: %s
Name: %s
Description: %s
Created At: %s
Released At: %s

`,
		release.TagName,
		release.Name,
		release.Description,
		release.CreatedAt,
		release.ReleasedAt,
	)

	if release.Assets != nil {
		for _, link := range release.Assets.Links {
			fmt.Fprintf(w, `
Asset::Link::Name: %s
Asset::Link::URL: %s

`,
				link.Name, link.URL)
		}
	}

	for _, m := range release.Milestones {
		fmt.Fprintf(w, `
Milestone: %s - %s

`, m.Title, m.Description)
	}

	fmt.Fprintf(w, "See all available releases here: %s/-/releases\n", os.Getenv("CI_PROJECT_URL"))
}
