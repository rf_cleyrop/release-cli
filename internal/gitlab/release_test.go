package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/testdata"
)

func TestClient_CreateRelease(t *testing.T) {
	releasedAt, err := ParseDateTime("2019-01-03T01:55:18.203Z")
	require.NoError(t, err)

	baseCRR := CreateReleaseRequest{
		ID:          "projectID",
		Name:        "release name",
		TagName:     "v0.1",
		Description: "release description",
		Ref:         "ref",
		ReleasedAt:  &releasedAt,
		Milestones:  []string{"v1.0"},
		Assets: &Assets{
			Links: []*Link{
				{
					Name:     "hoge",
					URL:      "https://gitlab.example.com/root/awesome-app/-/tags/v0.11.1/binaries/linux-amd64",
					Filepath: "/binaries/linux-amd64",
					LinkType: "other",
				},
			},
		},
	}

	tests := map[string]struct {
		res             func() *http.Response
		wantResponse    *CreateReleaseResponse
		wantErrResponse *ErrorResponse
	}{
		"success": {
			res: testdata.Responses[testdata.ResponseCreateReleaseSuccess],
			wantResponse: func() *CreateReleaseResponse {
				var res CreateReleaseResponse
				err := json.Unmarshal([]byte(testdata.CreateReleaseSuccessResponse), &res)
				require.NoError(t, err)
				return &res
			}(),
		},
		"unauthorized": {
			res:             testdata.Responses[testdata.ResponseUnauthorized],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusUnauthorized, Message: "401 Unauthorized"},
		},
		"forbidden": {
			res:             testdata.Responses[testdata.ResponseForbidden],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusForbidden, Message: "403 Forbidden"},
		},
		"bad_request": {
			res:             testdata.Responses[testdata.ResponseBadRequest],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusBadRequest, Message: "tag_name is missing"},
		},
		"conflict": {
			res:             testdata.Responses[testdata.ResponseConflict],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusConflict, Message: "Release already exists"},
		},
		"internal_server_error": {
			res:             testdata.Responses[testdata.ResponseInternalError],
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Message: "500 Internal Server Error"},
		},
		"unexpected_error": {
			res: func() *http.Response {
				return &http.Response{
					StatusCode: http.StatusInternalServerError,
					Body:       io.NopCloser(strings.NewReader(testdata.UnexpectedErrorResponse)),
				}
			},
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Err: "Something went wrong"},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			mhc := &MockHTTPClient{}
			res := tt.res()
			mhc.On("Do", mock.Anything).Return(res, nil).Once()
			defer require.NoError(t, res.Body.Close())

			gc := &Client{
				baseURL:    "http://127.0.0.1",
				jobToken:   "jobToken",
				projectID:  "projectID",
				httpClient: mhc,
			}
			got, err := gc.CreateRelease(context.Background(), &baseCRR)
			if tt.wantErrResponse != nil {
				require.Error(t, err)
				require.EqualError(t, err, tt.wantErrResponse.Error())

				return
			}

			require.NoError(t, err)
			require.NotNil(t, got)
			mhc.AssertExpectations(t)

			require.Equal(t, tt.wantResponse.Name, got.Name)
			require.Equal(t, tt.wantResponse.TagName, got.TagName)
			require.Equal(t, tt.wantResponse.Description, got.Description)
			require.NotEmpty(t, got.DescriptionHTML)
			require.Len(t, got.Assets.Links, 1)
			require.Equal(t, tt.wantResponse.Assets.Links[0].Name, got.Assets.Links[0].Name)
			require.Equal(t, tt.wantResponse.Assets.Links[0].URL, got.Assets.Links[0].URL)
			require.Contains(t, got.Assets.Links[0].URL, tt.wantResponse.Assets.Links[0].Filepath)
			require.Equal(t, tt.wantResponse.Assets.Links[0].LinkType, got.Assets.Links[0].LinkType)
			require.Equal(t, tt.wantResponse.ReleasedAt.String(), got.ReleasedAt.String())
			require.Len(t, got.Milestones, 1)
			require.Equal(t, tt.wantResponse.Milestones[0].Title, got.Milestones[0].Title)
			require.Equal(t, tt.wantResponse.Milestones[0].DueDate, got.Milestones[0].DueDate)
		})
	}
}

func TestClient_CreateRelease_NonAPIErrors(t *testing.T) {
	tests := map[string]struct {
		res        *http.Response
		err        error
		wantErrMsg string
	}{
		"failed_to_call_api": {
			err:        fmt.Errorf("something went wrong"),
			wantErrMsg: "failed to do request:",
		},
		"not_json_error_response": {
			res: &http.Response{
				StatusCode: http.StatusBadRequest,
				Body:       io.NopCloser(strings.NewReader("<not json>")),
			},
			wantErrMsg: "failed to decode error response:",
		},
		"not_json_success_response": {
			res: &http.Response{
				StatusCode: http.StatusCreated,
				Body:       io.NopCloser(strings.NewReader("<not json>")),
			},
			wantErrMsg: "failed to decode response:",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			mhc := &MockHTTPClient{}
			mhc.On("Do", mock.Anything).Return(tt.res, tt.err).Once()
			client, err := New("", "job-token", "", "projectID", mhc)
			require.NoError(t, err)

			res, err := client.CreateRelease(context.Background(), &CreateReleaseRequest{})
			require.Nil(t, res)
			require.Error(t, err)

			require.Contains(t, err.Error(), tt.wantErrMsg)

			if tt.res != nil {
				mhc.AssertExpectations(t)
			}
		})
	}
}

func TestClient_GetRelease(t *testing.T) {
	tests := map[string]struct {
		withHTML        bool
		mock            *MockHTTPClient
		wantResponse    *GetReleaseResponse
		wantErrResponse *ErrorResponse
	}{
		"success": {
			mock: func() *MockHTTPClient {
				mhc := &MockHTTPClient{}
				mhc.On("Do",
					mock.MatchedBy(func(req *http.Request) bool {
						return req.URL.Path == "/projects/projectID/releases/v0.1" && req.Method == http.MethodGet
					})).
					Return(testdata.Responses[testdata.ResponseGetReleaseSuccess](), nil).
					Once()

				return mhc
			}(),
			wantResponse: func() *GetReleaseResponse {
				var res GetReleaseResponse
				err := json.Unmarshal([]byte(testdata.GetReleaseResponseSuccess), &res)
				require.NoError(t, err)

				return &res
			}(),
		},
		"success_with_html": {
			withHTML: true,
			mock: func() *MockHTTPClient {
				mhc := &MockHTTPClient{}
				mhc.On("Do",
					mock.MatchedBy(func(req *http.Request) bool {
						includeHTML, err := strconv.ParseBool(req.URL.Query().Get("include_html_description"))
						require.NoError(t, err)

						return includeHTML
					})).
					Return(testdata.Responses[testdata.ResponseGetReleaseSuccessWithHTML](), nil).
					Once()

				return mhc
			}(),
			wantResponse: func() *GetReleaseResponse {
				var res GetReleaseResponse
				err := json.Unmarshal([]byte(testdata.GetReleaseResponseSuccessWithHTML), &res)
				require.NoError(t, err)

				return &res
			}(),
		},
		"forbidden": {
			mock: func() *MockHTTPClient {
				mhc := &MockHTTPClient{}
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseForbidden](), nil).Once()

				return mhc
			}(),
			wantErrResponse: &ErrorResponse{statusCode: http.StatusForbidden, Message: "403 Forbidden"},
		},
		"internal_server_error": {
			mock: func() *MockHTTPClient {
				mhc := &MockHTTPClient{}
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseInternalError](), nil).Once()

				return mhc
			}(),
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Message: "500 Internal Server Error"},
		},
		"unexpected_error": {
			mock: func() *MockHTTPClient {
				mhc := &MockHTTPClient{}
				mhc.On("Do", mock.Anything).Return(testdata.Responses[testdata.ResponseUnexpectedError](), nil).Once()

				return mhc
			}(),
			wantErrResponse: &ErrorResponse{statusCode: http.StatusInternalServerError, Err: "Something went wrong"},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			gc := &Client{
				baseURL:    "http://127.0.0.1",
				jobToken:   "jobToken",
				projectID:  "projectID",
				httpClient: tt.mock,
			}

			got, err := gc.GetRelease(context.Background(), "v0.1", tt.withHTML)
			if tt.wantErrResponse != nil {
				require.Error(t, err)
				require.EqualError(t, err, tt.wantErrResponse.Error())

				return
			}

			require.NoError(t, err)
			require.NotNil(t, got)
			tt.mock.AssertExpectations(t)

			require.Equal(t, tt.wantResponse.Name, got.Name)
			require.Equal(t, tt.wantResponse.TagName, got.TagName)
			require.Equal(t, tt.wantResponse.Description, got.Description)
			require.Equal(t, tt.wantResponse.DescriptionHTML, got.DescriptionHTML)
			require.Equal(t, tt.wantResponse.CreatedAt, got.CreatedAt)
			require.Equal(t, tt.wantResponse.ReleasedAt, got.ReleasedAt)
			require.Equal(t, tt.wantResponse.CommitPath, got.CommitPath)
			require.Equal(t, tt.wantResponse.TagPath, got.TagPath)

			require.NotNil(t, got.Assets)
			require.Len(t, got.Assets.Links, len(tt.wantResponse.Assets.Links))
			require.Len(t, got.Milestones, len(tt.wantResponse.Milestones))
			require.Len(t, got.Evidences, len(tt.wantResponse.Evidences))

			require.NotNil(t, got.Author)
			require.EqualValues(t, tt.wantResponse.Author, got.Author)
			require.NotNil(t, got.Commit)
			require.EqualValues(t, tt.wantResponse.Commit, got.Commit)
		})
	}
}
